import click
import pandas as pd


@click.command()
@click.argument('input_filepath', type=click.Path())
@click.argument('output_filepath', type=click.Path())
def useless_func_for_hw(input_filepath, output_filepath):
    df = pd.read_csv(input_filepath)

    df.to_csv(output_filepath)
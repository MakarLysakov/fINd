import time
import hmac
import base64
import hashlib
import requests
import json
import uuid
from Tools_v1 import PoloniexToolsFutures

def current_price():
    url = 'https://futures-api.poloniex.com/api/v1/level2/depth?symbol=BTCUSDTPERP&depth=depth5'
    response = requests.request('get', url)
    return str(round((float(response.json()['data']['asks'][0][0]) + float(response.json()['data']['bids'][0][0])) / 2, 1))

    
t1 = PoloniexToolsFutures()

#Поступил сигнал на покупку
buy_price = current_price()
take_profit = buy_price * 1.015
stop_loss = buy_price * 0.99
size = "1"
expect_size = size

t1.createOrder("buy", size, str(buy_price))
start = int(time.time())

#исполнилась полностью
#исполнилась частично
#не исполнилась
while True:
    time.sleep(5)
    now = int(time.time())
    #исполнилась менее, чем за 30 минут
    if (t1.getOrderList().json()['data']['totalNum'] == 0) and ((now - start) <= 1800):
        flag = 1
        break
    if (t1.getOrderList().json()['data']['items'][0]['dealValue'] != '0') and ((now - start) > 1800):
        flag = 1
        expect_size = t1.getOrderList().json()['data']['items'][0]['dealValue']
        t1.cancelAllOrders()
        break
    if (t1.getOrderList().json()['data']['items'][0]['dealValue'] == '0') and ((now - start) > 1800):
        flag = 0
        t1.cancelAllOrders()
        break
    

inner_flag = ""
while flag:
    time.sleep(5)
    current_price = float(requests.request('get', 
                                   'https://futures-api.poloniex.com/api/v1/mark-price/BTCUSDTPERP/current'
                                  ).json()['data']['indexPrice']

    if current_price >= take_profit and inner_flag != "take":
        inner_flag = "take"
        t1.cancelAllOrders()
        t1.createOrder("sell", expect_size, str(current_price*0.999))

    if current_price <= stop_loss and inner_flag != "stop":
        inner_flag = "stop"
        t1.cancelAllOrders()
        t1.createOrder("sell", expect_size, str(current_price*0.999))

    #условие выхода из цикла (нет активных ордеров, inner_flag был изменен)
    if t1.getOrderList().json()['data']['totalNum'] == 0 and inner_flag != "":
        break
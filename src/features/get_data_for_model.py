from Tools_v1 import poloniexTools
import hmac
import base64
import hashlib
import requests
import json
import uuid
import time
from polosdk import RestClient
from datetime import datetime
import pandas as pd

t1 = poloniexTools()
prime = pd.DataFrame(columns=['startTime', 'low', 'high', 'open', 'close', 'amount', 'quantity'])

while True:
    time.sleep(300)
    now = int(time.time())
    endtime = str(datetime.fromtimestamp(now)).replace('-', '/')
        
    start = now-300
    starttime = str(datetime.fromtimestamp(start)).replace('-', '/')

    prime.loc[len(prime.index)] = t1.get_data('MINUTE_5', starttime_p=str(starttime), endtime_p=str(endtime)).iloc[0]
    
    if len(prime.index) > 200:
        prime = prime.drop(axis=0, index=0)

    prime.to_csv('fresh_data.csv', sep=';')   
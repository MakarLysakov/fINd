import time
from polosdk import RestClient
from datetime import datetime
import pandas as pd
import click

@click.command()
@click.argument('output_filepath', type=click.Path())
def get_data(output_filepath) -> pd.core.frame.DataFrame:
    """
    Возвращает заданное количество свеч с данного момента с заданым интервалом

    Параметры
    ---------
    inter : str
        задается интервал заданных свеч
    starttime_p : str
        задается начальная точка отсчета
    endtime_p : str
        задается конченая точка отсчета

    Возвращаемое значение
    ---------------------
    pd.core.frame.DataFrame
    """
    inter = "MINUTE_5"
    starttime_p = "2022/10/10 20:21:21"
    endtime_p = "2022/10/11 20:21:21"
    client = RestClient()
    interval_pool = {'MINUTE_1': 60, 'MINUTE_5': 300, 'HOUR_1': 3_600, 'DAY_1': 86_400, 'WEEK_1': 604_800}

    starttime = int(datetime.timestamp(datetime.strptime(starttime_p, "%Y/%m/%d %H:%M:%S")))
    endtime = int(datetime.timestamp(datetime.strptime(endtime_p, "%Y/%m/%d %H:%M:%S")))
    amount = ((endtime - starttime) // interval_pool[inter])

    data = []

    maxReqCount = 10
    for i in range(amount // 100):
        j = 0
        while j < maxReqCount:
            try:
                response = client.markets().get_candles(symbol='BTC_USDT',
                                                        interval=inter,
                                                        start_time=(endtime - 1 - 100 * interval_pool[inter]) * 1_000,
                                                        end_time=endtime * 1_000)
            except:
                j += 1
            else:
                endtime -= interval_pool[inter] * 100
                for value in range(len(response) - 1, -1, -1):
                    data.append(response[value])
                time.sleep(0.2)
                j = maxReqCount

    j = 0
    while j < maxReqCount:
        try:
            response = client.markets().get_candles(symbol='BTC_USDT',
                                                    interval=inter,
                                                    start_time=(endtime - 1 - amount % 100 * interval_pool[
                                                        inter]) * 1_000,
                                                    end_time=endtime * 1_000)
        except:
            j += 1
        else:
            j = maxReqCount

    for value in range(len(response) - 1, -1, -1):
        data.append(response[value])

    dataFrame = pd.DataFrame(data, columns=['low', 'high', 'open', 'close', 'amount', 'quantity',
                                            'buyTakerAmount', 'buyTakerQuantity', 'tradeCount',
                                            'ts', 'weightedAverage', 'interval', 'startTime', 'closeTime'])
    dataFrame['startTime'] = list(
        map(datetime.fromtimestamp, map(lambda number: int(number) // 1000, dataFrame['startTime'])))
    dataFrame['closeTime'] = list(
        map(datetime.fromtimestamp, map(lambda number: int(number) // 1000, dataFrame['closeTime'])))
    dataFrame = dataFrame.drop(dataFrame.columns[[6, 7, 8, 9, 10, 11, 13]], axis=1)
    dataFrame = dataFrame[['startTime', 'low', 'high', 'open', 'close', 'amount', 'quantity']]
    dataFrame = dataFrame.iloc[::-1].reset_index(drop=True)
    dataFrame.to_csv(output_filepath)
